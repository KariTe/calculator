package com;

import com.calculator.CalcType;
import com.calculator.ClearMemoryButtonsActionHandler;
import com.calculator.Memory;
import com.calculator.listeners.*;


import javax.swing.*;

public class CalcWindow {

    public JPanel panel1;
    private JPanel resultWindow;
    private JButton plusButton;
    private JButton minusButton;
    private JButton multiplyButton;
    private JButton divideButton;
    private JButton a0Button;
    private JButton a1Button;
    private JButton a2Button;
    private JButton a3Button;
    private JButton a4Button;
    private JButton a5Button;
    private JButton a7Button;
    private JButton a6Button;
    private JButton a8Button;
    private JButton a9Button;
    private JButton resultButton;
    private JButton dotButton;
    private JButton reverseSignButton;
    private JButton clearButton;
    private JButton acButton;
    private JTextArea textArea1;


    public CalcWindow() {
        initButtons();
        initMemory();
    }

    void initButtons() {

        a0Button.addActionListener(new NumberButtonActionListener("0", textArea1, clearButton, acButton));
        a1Button.addActionListener(new NumberButtonActionListener("1", textArea1, clearButton, acButton));
        a2Button.addActionListener(new NumberButtonActionListener("2", textArea1, clearButton, acButton));
        a3Button.addActionListener(new NumberButtonActionListener("3", textArea1, clearButton, acButton));
        a4Button.addActionListener(new NumberButtonActionListener("4", textArea1, clearButton, acButton));
        a5Button.addActionListener(new NumberButtonActionListener("5", textArea1, clearButton, acButton));
        a6Button.addActionListener(new NumberButtonActionListener("6", textArea1, clearButton, acButton));
        a7Button.addActionListener(new NumberButtonActionListener("7", textArea1, clearButton, acButton));
        a8Button.addActionListener(new NumberButtonActionListener("8", textArea1, clearButton, acButton));
        a9Button.addActionListener(new NumberButtonActionListener("9", textArea1, clearButton, acButton));

        ClearMemoryButtonsActionHandler clearMemoryButtonsActionHandler = new ClearMemoryButtonsActionHandler(clearButton, acButton);

        plusButton.addActionListener(new OperationButtonActionListener(CalcType.ADD, textArea1));
        minusButton.addActionListener(new OperationButtonActionListener(CalcType.SUBTRACT, textArea1));
        multiplyButton.addActionListener(new OperationButtonActionListener(CalcType.MULTIPLY, textArea1));
        divideButton.addActionListener(new OperationButtonActionListener(CalcType.DIVIDE,textArea1));
        dotButton.addActionListener(new DotButtonActionListener(".", textArea1));
        reverseSignButton.addActionListener(new ReverseSignButtonActionListener(CalcType.NEGATE, textArea1));
        acButton.addActionListener(new ClearAllButtonActionListener(textArea1));
        clearButton.addActionListener(new ClearButtonActionListener(clearMemoryButtonsActionHandler, textArea1));
        resultButton.addActionListener(new ResultButtonActionListener(textArea1));

    }

    void initMemory(){
        Memory.INSTANCE.setLastOperation(CalcType.NONE);
    }
}
