package com.calculator.commands.factories;

import com.calculator.commands.DivideCommand;
import com.calculator.commands.OperationCommand;

import java.math.BigDecimal;

public class DivideCommandFactory implements AbstractOperationCommandFactory {
    @Override
    public OperationCommand newOperationCommandInstance(BigDecimal element1, BigDecimal element2) {
        return new DivideCommand(element1, element2);
    }
}
