package com.calculator.commands.factories;

import com.calculator.commands.MultiplyCommand;
import com.calculator.commands.OperationCommand;

import java.math.BigDecimal;

public class MultiplyCommandFactory implements AbstractOperationCommandFactory {
    @Override
    public OperationCommand newOperationCommandInstance(BigDecimal element1, BigDecimal element2) {
        return new MultiplyCommand(element1, element2);
    }
}
