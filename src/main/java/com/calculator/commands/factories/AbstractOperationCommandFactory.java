package com.calculator.commands.factories;

import com.calculator.commands.OperationCommand;

import java.math.BigDecimal;

public interface AbstractOperationCommandFactory {
    OperationCommand newOperationCommandInstance(BigDecimal element1, BigDecimal element2);
}
