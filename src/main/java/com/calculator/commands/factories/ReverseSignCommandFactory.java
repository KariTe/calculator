package com.calculator.commands.factories;

import com.calculator.commands.OperationCommand;
import com.calculator.commands.ReverseSignCommand;

import java.math.BigDecimal;

public class ReverseSignCommandFactory implements AbstractOperationCommandFactory {
    @Override
    public OperationCommand newOperationCommandInstance(BigDecimal element1, BigDecimal element2) {
        return new ReverseSignCommand(element1, element2);
    }
}
