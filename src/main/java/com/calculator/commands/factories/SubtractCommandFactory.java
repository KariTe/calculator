package com.calculator.commands.factories;

import com.calculator.commands.OperationCommand;
import com.calculator.commands.SubtractCommand;

import java.math.BigDecimal;

public class SubtractCommandFactory implements AbstractOperationCommandFactory {
    @Override
    public OperationCommand newOperationCommandInstance(BigDecimal element1, BigDecimal element2) {
        return new SubtractCommand(element1, element2);
    }
}
