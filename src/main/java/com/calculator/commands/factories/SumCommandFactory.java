package com.calculator.commands.factories;

import com.calculator.commands.OperationCommand;
import com.calculator.commands.SumCommand;

import java.math.BigDecimal;

public class SumCommandFactory implements AbstractOperationCommandFactory {
    @Override
    public OperationCommand newOperationCommandInstance(BigDecimal element1, BigDecimal element2) {
        return new SumCommand(element1, element2);
    }

}
