package com.calculator.commands;

import com.calculator.CalcType;
import com.calculator.commands.factories.*;

import java.math.BigDecimal;

public class FactoryProvider {

    private static final SumCommandFactory SUM_COMMAND_FACTORY = new SumCommandFactory();
    private static final SubtractCommandFactory SUBTRACT_COMMAND_FACTORY = new SubtractCommandFactory();
    private static final MultiplyCommandFactory MULTIPLY_COMMAND_FACTORY = new MultiplyCommandFactory();
    private static final DivideCommandFactory DIVIDE_COMMAND_FACTORY = new DivideCommandFactory();
    private static final ReverseSignCommandFactory REVERSE_SIGN_COMMAND_FACTORY = new ReverseSignCommandFactory();

    private FactoryProvider() {
    }

    public static OperationCommand newOperationCommandInstance(CalcType calcType, BigDecimal element1, BigDecimal element2) {
        switch (calcType) {
            case ADD: return SUM_COMMAND_FACTORY.newOperationCommandInstance(element1, element2);
            case SUBTRACT: return SUBTRACT_COMMAND_FACTORY.newOperationCommandInstance(element1, element2);
            case MULTIPLY: return MULTIPLY_COMMAND_FACTORY.newOperationCommandInstance(element1, element2);
            case DIVIDE: return DIVIDE_COMMAND_FACTORY.newOperationCommandInstance(element1, element2);
            case NEGATE: return REVERSE_SIGN_COMMAND_FACTORY.newOperationCommandInstance(element1, element2);
            default: throw new UnsupportedOperationException();
        }
    }

}
