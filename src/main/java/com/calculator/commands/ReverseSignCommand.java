package com.calculator.commands;

import java.math.BigDecimal;

public class ReverseSignCommand extends OperationCommand{
    public ReverseSignCommand(BigDecimal firstElement, BigDecimal secondElement) {
        super(firstElement, secondElement);
    }

    @Override
    public BigDecimal execute() {
        return getFirstElement().negate();
    }
}
