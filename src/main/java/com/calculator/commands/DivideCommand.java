package com.calculator.commands;

import java.math.BigDecimal;

public class DivideCommand extends OperationCommand {
    public DivideCommand(BigDecimal firstElement, BigDecimal secondElement) {
        super(firstElement, secondElement);
    }

    @Override
    public BigDecimal execute() {
        return getFirstElement().divide(getSecondElement());
    }
}
