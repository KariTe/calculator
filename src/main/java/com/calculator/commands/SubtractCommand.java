package com.calculator.commands;

import java.math.BigDecimal;

public class SubtractCommand extends OperationCommand {
    public SubtractCommand(BigDecimal firstElement, BigDecimal secondElement) {
        super(firstElement, secondElement);
    }

    @Override
    public BigDecimal execute() {
        return getFirstElement().subtract(getSecondElement());
    }

}
