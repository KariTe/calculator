package com.calculator.commands;

import com.calculator.CalcType;

import java.math.BigDecimal;

public class CommandProcessor {

    private static CommandProcessor instance;

    public static CommandProcessor getInstance(){

        if (instance == null){
            synchronized (CommandProcessor.class) {
                if (instance == null){
                    instance = new CommandProcessor();
                }
            }
        }
        return instance;
    }

    public BigDecimal process(CalcType calcType, BigDecimal elem1, BigDecimal elem2) {
        OperationCommand command = FactoryProvider.newOperationCommandInstance(calcType, elem1, elem2);

        long t = System.currentTimeMillis();
        BigDecimal res = command.execute();
        long time = System.currentTimeMillis() - t;

        System.out.println("Execution time of " +  command.getClass().getSimpleName() +" :" + time + "ms");
        return res;

    }

}


