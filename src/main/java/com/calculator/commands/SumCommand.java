package com.calculator.commands;

import java.math.BigDecimal;

public class SumCommand extends OperationCommand {
    public SumCommand(BigDecimal firstElement, BigDecimal secondElement) {
        super(firstElement, secondElement);
    }

    @Override
    public BigDecimal execute() {
        return getFirstElement().add(getSecondElement());
    }
}
