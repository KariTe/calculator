package com.calculator.commands;

import java.math.BigDecimal;

public abstract class OperationCommand {

    private final BigDecimal firstElement;

    private final BigDecimal secondElement;

    public OperationCommand(BigDecimal firstElement, BigDecimal secondElement) {
        this.firstElement = firstElement;
        this.secondElement = secondElement;
    }

    public abstract BigDecimal execute();

    public BigDecimal getFirstElement() {
        return firstElement;
    }

    public BigDecimal getSecondElement() {
        return secondElement;
    }
}
