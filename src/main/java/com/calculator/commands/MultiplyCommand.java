package com.calculator.commands;

import java.math.BigDecimal;

public class MultiplyCommand extends OperationCommand {
    public MultiplyCommand(BigDecimal firstElement, BigDecimal secondElement) {
        super(firstElement, secondElement);
    }

    @Override
    public BigDecimal execute() {
        return getFirstElement().multiply(getSecondElement());
    }
}
