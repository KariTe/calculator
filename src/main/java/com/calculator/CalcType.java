package com.calculator;

public enum CalcType {
    ADD, SUBTRACT, MULTIPLY, DIVIDE, NEGATE, RESULT, NONE
}