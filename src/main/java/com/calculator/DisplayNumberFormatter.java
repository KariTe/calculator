package com.calculator;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class DisplayNumberFormatter {

    private static final DecimalFormat NUMBER_FORMAT;

    static {
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setDecimalSeparator('.');
        decimalFormatSymbols.setGroupingSeparator(' ');

        NUMBER_FORMAT = new DecimalFormat();
        NUMBER_FORMAT.setDecimalFormatSymbols(decimalFormatSymbols);
        NUMBER_FORMAT.setMaximumFractionDigits(20);
    }

    private DisplayNumberFormatter() {
    }

    public static String intoDisplayFormat(String number){
        String numberWithoutWhiteSpaces = removeWhiteSpaces(number);
        return NUMBER_FORMAT.format(new BigDecimal(numberWithoutWhiteSpaces));
    }

    public static String fromDisplayFormat(String number) {
        return removeWhiteSpaces(number);
    }

    public static String removeWhiteSpaces(String input){
        return StringUtils.deleteWhitespace(input);
    }
}


