package com.calculator;

import java.math.BigDecimal;

public enum Memory {

    INSTANCE;

    private BigDecimal firstNumber;
    private BigDecimal secondNumber;
    private BigDecimal resultNumber;
    private CalcType lastOperation;
    private boolean clearOn;


    public void setFirstNumber(BigDecimal displayedNumber) {
        this.firstNumber = displayedNumber;
    }

    public void setSecondNumber(BigDecimal secondNumber) {
        this.secondNumber = secondNumber;
    }

    public void setResultNumber(BigDecimal resultNumber) {
        this.resultNumber = resultNumber;
    }

    public void setLastOperation(CalcType lastOperation) {
        this.lastOperation = lastOperation;
    }

    public void setClearOn(boolean clearOn) {
        this.clearOn = clearOn;
    }

    public void clearAll() {
        setClearOn(false);
    }

    public MemoryState fetchMemory() {
        return new MemoryState(
                this.firstNumber,
                this.secondNumber,
                this.resultNumber,
                this.lastOperation,
                this.clearOn
        );
    }

    public static class MemoryState{

        private final BigDecimal firstNumber;
        private final BigDecimal secondNumber;
        private final BigDecimal resultNumber;
        private final CalcType lastOperation;
        private final boolean clearOn;

        private MemoryState(BigDecimal firstNumber, BigDecimal secondNumber, BigDecimal resultNumber, CalcType lastOperation, boolean clearOn) {
            this.firstNumber = firstNumber;
            this.secondNumber = secondNumber;
            this.resultNumber = resultNumber;
            this.lastOperation = lastOperation;
            this.clearOn = clearOn;
        }

        public BigDecimal getFirstNumber() {
            return firstNumber;
        }

        public BigDecimal getSecondNumber() {
            return secondNumber;
        }

        public BigDecimal getResultNumber() {
            return resultNumber;
        }

        public CalcType getLastOperation() {
            return lastOperation;
        }

        public boolean isClearOn() {
            return clearOn;
        }
    }

}
