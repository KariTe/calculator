package com.calculator.listeners;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class DotButtonActionListener implements ActionListener {

    private final String sign;
    private final JTextArea display;

    public DotButtonActionListener(String sign, JTextArea display) {
        this.sign = sign;
        this.display = display;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        display.append(sign);
    }
}
