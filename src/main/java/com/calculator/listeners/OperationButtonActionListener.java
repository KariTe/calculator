package com.calculator.listeners;

import com.calculator.CalcType;
import com.calculator.DisplayNumberFormatter;
import com.calculator.Memory;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

public class OperationButtonActionListener implements ActionListener {

    private static final String EMPTY_STRING = "";
    private final CalcType calcType;
    private final JTextArea display;

    public OperationButtonActionListener(CalcType calcType, JTextArea display) {
        this.calcType = calcType;
        this.display = display;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        BigDecimal number = new BigDecimal(DisplayNumberFormatter.fromDisplayFormat(display.getText()));
        Memory.INSTANCE.setFirstNumber(number);
        Memory.INSTANCE.setLastOperation(calcType);
        display.setText(EMPTY_STRING);
    }

    protected JTextArea getDisplay() {
        return display;
    }
}
