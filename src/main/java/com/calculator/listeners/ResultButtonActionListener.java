package com.calculator.listeners;

import com.calculator.CalcType;
import com.calculator.DisplayNumberFormatter;
import com.calculator.Memory;
import com.calculator.commands.CommandProcessor;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.Optional;

public class ResultButtonActionListener implements ActionListener {

    private final JTextArea display;

    public ResultButtonActionListener(JTextArea display) {
        this.display = display;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        Memory.INSTANCE.setSecondNumber(transformDisplayedNumber());
        Memory.MemoryState memoryState = Memory.INSTANCE.fetchMemory();

        try {
            String result = memoryState.isClearOn() ? calcAfterClear(memoryState) : calc(memoryState);

            display.setText(result);
            Memory.INSTANCE.setClearOn(false);
            Memory.INSTANCE.setResultNumber(displayedNumber());
            Memory.INSTANCE.setLastOperation(CalcType.RESULT);

        } catch (IllegalStateException | ArithmeticException ex) {
            display.setText(ex.getMessage());
        }
    }

    private BigDecimal transformDisplayedNumber() {
        return Optional.ofNullable(DisplayNumberFormatter.fromDisplayFormat(display.getText()))
                .filter(s -> s.length() > 0)
                .map(BigDecimal::new)
                .orElse(BigDecimal.ZERO);
    }

    private String calculate(CalcType lastOperation, BigDecimal number, BigDecimal number2) {
        BigDecimal res = CommandProcessor.getInstance().process(lastOperation, number, number2);
        return  DisplayNumberFormatter.intoDisplayFormat(res.toString());
    }

    private BigDecimal displayedNumber() {
        return new BigDecimal(DisplayNumberFormatter.fromDisplayFormat(display.getText()));
    }

    private String calc(Memory.MemoryState memoryState) {
        String result = calculate(memoryState.getLastOperation(), memoryState.getFirstNumber(), memoryState.getSecondNumber());
        Memory.INSTANCE.setLastOperation(CalcType.RESULT);
        return  result;
    }

    private String calcAfterClear(Memory.MemoryState memoryState) {
        return calculate(memoryState.getLastOperation(), memoryState.getResultNumber(), memoryState.getSecondNumber());
    }

}
