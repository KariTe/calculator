package com.calculator.listeners;

import com.calculator.ClearMemoryButtonsActionHandler;
import com.calculator.Memory;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ClearButtonActionListener implements ActionListener {

    private final ClearMemoryButtonsActionHandler clearMemoryButtonsActionHandler;
    private final JTextArea display;
    private static final String CLEAR_DISPLAY = "0";

    public ClearButtonActionListener(ClearMemoryButtonsActionHandler clearMemoryButtonsActionHandler, JTextArea display){
        this.clearMemoryButtonsActionHandler = clearMemoryButtonsActionHandler;
        this.display = display;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Memory.INSTANCE.setClearOn(true);
        display.setText(CLEAR_DISPLAY);
        clearMemoryButtonsActionHandler.switchClearButtons();
    }
}
