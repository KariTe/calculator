package com.calculator.listeners;

import com.calculator.CalcType;
import com.calculator.DisplayNumberFormatter;
import com.calculator.commands.CommandProcessor;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;

public class ReverseSignButtonActionListener extends OperationButtonActionListener {

    public ReverseSignButtonActionListener(CalcType calcType, JTextArea display) {
        super(calcType, display);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        BigDecimal number = new BigDecimal(DisplayNumberFormatter.intoDisplayFormat(getDisplay().getText()));
        BigDecimal res = CommandProcessor.getInstance().process(CalcType.NEGATE, number, null);
        String formattedReverse = DisplayNumberFormatter.intoDisplayFormat(res.toString());
        getDisplay().setText(formattedReverse);

    }
}
