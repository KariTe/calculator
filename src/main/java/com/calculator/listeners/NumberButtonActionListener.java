package com.calculator.listeners;

import com.calculator.CalcType;
import com.calculator.DisplayNumberFormatter;
import com.calculator.Memory;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NumberButtonActionListener implements ActionListener  {

    private static final String CLEAR_DISPLAY = "0";

    private final String digit;
    private final JTextArea display;
    private final JButton clear;
    private final JButton clearAll;

    public NumberButtonActionListener(String digit, JTextArea display, JButton clear, JButton clearAll) {
        this.digit = digit;
        this.display = display;
        this.clear = clear;
        this.clearAll = clearAll;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (Memory.INSTANCE.fetchMemory().getLastOperation().equals(CalcType.RESULT)){
            display.setText(CLEAR_DISPLAY);
        }
        calc();
        clearAll.setVisible(false);
        clear.setVisible(true);
    }

    private void calc(){
        String formattedNumber = DisplayNumberFormatter.intoDisplayFormat(display.getText() + digit);
        display.setText(formattedNumber);
    }
}
