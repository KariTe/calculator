package com.calculator.listeners;

import com.calculator.Memory;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ClearAllButtonActionListener implements ActionListener {

    private final JTextArea display;
    private static final String CLEAR_DISPLAY = "0";

    public ClearAllButtonActionListener(JTextArea display){
        this.display = display;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Memory.INSTANCE.clearAll();
        display.setText(CLEAR_DISPLAY);
    }
}
