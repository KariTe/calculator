package com.calculator;

import javax.swing.*;

public class ClearMemoryButtonsActionHandler {

    private final JButton clear;
    private final JButton clearAll;

    public ClearMemoryButtonsActionHandler(JButton clear, JButton clearAll) {
        this.clear = clear;
        this.clearAll = clearAll;
    }

    public void switchClearButtons(){
        clear.setVisible(clearAll.isVisible());
        clearAll.setVisible(!clearAll.isVisible());
    }
}
