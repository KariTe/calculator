package com.calculator.commands;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class DivideCommandTest {

    @Test
    public void GivenDivideCommand10And1_WhenExecute_ThenResult10(){
        // Given
        DivideCommand divideCommand = new DivideCommand(BigDecimal.TEN, BigDecimal.ONE);

        // When
        BigDecimal res = divideCommand.execute();

        // Then
        assertEquals(res, BigDecimal.TEN);

    }

    @Test(expected = ArithmeticException.class)
    public void GivenDivideCommand10And1_WhenExecute_ThenArithmeticExceptionIsThrow(){
        // Given
        DivideCommand divideCommand = new DivideCommand(BigDecimal.TEN, BigDecimal.ZERO);
        divideCommand.execute();

    }

}