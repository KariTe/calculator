package com.calculator.commands;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class ReverseSignCommandTest {

    @Test
    public void GivenFirstElement10_WhenExecute_ThenResultMinusTen () {
        // Given
        BigDecimal element1 = BigDecimal.TEN;


        // When
        BigDecimal result = element1.negate();

        // Then
        assertNotNull(result);
        assertEquals(result,new BigDecimal(10).negate());




    }
}