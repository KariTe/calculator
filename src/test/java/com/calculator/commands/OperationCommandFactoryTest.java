package com.calculator.commands;

import com.calculator.CalcType;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class OperationCommandFactoryTest {

    @Test
    public void testMethod() {
        // Given
        BigDecimal elem1 = BigDecimal.TEN;
        BigDecimal elem2 = BigDecimal.ONE;
        CalcType calcType = CalcType.ADD;

        // When
        OperationCommand command = FactoryProvider.newOperationCommandInstance(calcType, elem1, elem2);

        // Then
        assertEquals(command.getClass(), SumCommand.class);
        assertEquals(command.getFirstElement(), elem1);
        assertEquals(command.getSecondElement(), elem2);
    }

}