package com.calculator.commands;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class SumCommandTest {

    @Test
    public void GivenSumCommandWith1And10_WhenExecute_ThenResult11() {
        // given
        SumCommand sumCommand = new SumCommand(BigDecimal.ONE, BigDecimal.TEN);

        // when
        BigDecimal result = sumCommand.execute();

        // then
        assertEquals(result, new BigDecimal(11));
    }

    @Test
    public void GivenSumCommandWithMinus1AndNegativeValue10_WhenExecute_ThenResultMinus3(){
        // given
        SumCommand sumCommand = new SumCommand(BigDecimal.ONE.negate(), BigDecimal.TEN.negate());

        // when
        BigDecimal res = sumCommand.execute();

        //then
        assertNotNull(res);
        assertEquals(res, new BigDecimal(11).negate());

    }



}