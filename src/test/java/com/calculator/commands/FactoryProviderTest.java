package com.calculator.commands;

import com.calculator.CalcType;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class FactoryProviderTest {

    public static final BigDecimal ELEMENT_1 = BigDecimal.TEN;
    public static final BigDecimal ELEMENT_2 = BigDecimal.ONE;

    @Test
    public void newOperationCommandInstance() {
        // Given
        CalcType calcType = CalcType.DIVIDE;

        // When
//        OperationCommand res = FactoryProvider.getInstance().newOperationCommandInstance(calcType, ELEMENT_1, ELEMENT_2);
        OperationCommand res = FactoryProvider.newOperationCommandInstance(calcType, ELEMENT_1, ELEMENT_2);

        // Then
        assertEquals(res.getClass(), DivideCommand.class);
        assertEquals(res.getFirstElement(), ELEMENT_1);
        assertEquals(res.getSecondElement(), ELEMENT_2);

    }
}