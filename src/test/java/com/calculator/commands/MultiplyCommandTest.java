package com.calculator.commands;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class MultiplyCommandTest {

    private static final BigDecimal TWO = new BigDecimal(2);
    public static final BigDecimal EXPECTED = new BigDecimal(20);


    @Test
    public void GivenMultiply10by2_WhenExecute_ThenResult10(){

        //Given
        MultiplyCommand multiplyCommand = new MultiplyCommand(BigDecimal.TEN, TWO);

        //When
        BigDecimal res = multiplyCommand.execute();

        //Then
        assertEquals(res, EXPECTED);

    }

}