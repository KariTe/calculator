package com.calculator.listeners;

import com.calculator.CalcType;
import org.junit.Test;

import javax.swing.*;

import java.awt.event.ActionEvent;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class ReverseSignButtonActionListenerIntegrationTest {

    @Test
    public void GivenBigDecimalNumber10_WhenActionPerformed_ResultMinus10() {
        // Given
        JTextArea display = new JTextArea();
        display.setText("2");
        ReverseSignButtonActionListener reverseSignButtonActionListener = new ReverseSignButtonActionListener(CalcType.NEGATE, display);

        // When
        reverseSignButtonActionListener.actionPerformed(new ActionEvent(new Object(), 2, ""));

        //Then
        assertEquals(display.getText(), "-2");

    }


}