## Calculator
_by Karol Telus_

This is the Calculator that has been written basing on the JAVA 8 and Swing technology. The Calculator's mechanics was created on the model of macOS calculator. Comparing to the original model the base key pad calculator was replaced with telephone touch-tone key. In this Calculator the following design patterns are used: 

- Command, 
- Singleton (enum, synchronised), 
- Abstract Factory, 
- Factory Method, 
- Observer (ActionListener).
